import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { lightTheme, darkTheme } from '../style/themes';
import { ITodoState } from '../types/types';

const initialState: ITodoState = {
    list: [],
    theme: lightTheme
}

const todoSlice = createSlice({
    name: 'todos',
    initialState,
    reducers: {
        addTodo(state, action: PayloadAction<string>) {
            state.list.push({
                id: Date.now(),
                title: action.payload,
                completed: false
            });
        },
        deleteTodo(state, action: PayloadAction<number>) {
            state.list = state.list.filter(todo => todo.id !== action.payload);
        },
        toggleTodo(state, action: PayloadAction<number>) {
            const toggledTodo = state.list.find(todo => todo.id === action.payload);
            if (toggledTodo) {
                toggledTodo.completed = !toggledTodo.completed;
            }
        },
        toggleTheme(state, action: PayloadAction<boolean>) {
            if (action.payload === true) {
                state.theme = darkTheme;
            } else {
                state.theme = lightTheme;
            }
        }
    }
});

export const {addTodo, deleteTodo, toggleTodo, toggleTheme} = todoSlice.actions;
export default todoSlice.reducer;