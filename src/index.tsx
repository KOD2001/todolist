import ReactDOM from 'react-dom/client'

import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/lib/integration/react';

import { App } from './components/App'
import { GlobalStyle } from './style/style';
import store, {persistor} from './store';

const root = ReactDOM.createRoot(document.getElementById('root') as HTMLElement)

root.render(
  <Provider store={store}>
    <PersistGate persistor={persistor}>
      <GlobalStyle />
      <App />
    </PersistGate>
  </Provider>
);
