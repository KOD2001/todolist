import { useState, useEffect, useRef } from 'react';
import { useAppDispatch, useAppSelector } from '../hook';
import { addTodo, toggleTheme } from '../store/todoSlice';
import { AppWrapper, CreateTaskWrapper, StyledButton, StyledHeader, StyledInput, StyledList, ThemeCheckbox, TodoListWrapper } from '../style/style';
import { TodoList } from './TodoList';
import { ThemeProvider } from 'styled-components';

const App: React.FC = () => {
    const [inputValue, setInputValue] = useState('');
    const dispatch = useAppDispatch();
    const theme = useAppSelector(state => state.todos.theme);

    const inputRef = useRef<HTMLInputElement>(null);

    useEffect(() => {
        if (inputRef.current) inputRef.current.focus();
    }, [])

    const inputChangeHandler: React.ChangeEventHandler<HTMLInputElement> = (e) => {
        setInputValue(e.target.value);
    }

    const addTask = () => {
        if (inputValue.trim()) {
            dispatch(addTodo(inputValue));
        }
        setInputValue('');
    }

    const handleKeyDown: React.KeyboardEventHandler<HTMLInputElement> = (e) => {
        if (e.key === 'Enter') {
            addTask();
        }
    }

    const handleThemeChange: React.ChangeEventHandler<HTMLInputElement> = (e) => {
        dispatch(toggleTheme(e.target.checked));
    }

    return (
        <ThemeProvider theme={theme}>
            <AppWrapper>
                <ThemeCheckbox>
                    <input checked={theme.name === 'dark'? true : false} onChange={handleThemeChange} type='checkbox'/>
                    <span className='slider'></span>
                </ThemeCheckbox>
                <TodoListWrapper>
                    <StyledHeader>My Tasks</StyledHeader>
                    <CreateTaskWrapper>
                        <StyledInput 
                        value={inputValue} 
                        onChange={inputChangeHandler}
                        onKeyDown={handleKeyDown}
                        ref={inputRef} 
                        placeholder='Create task'
                        type='text' name='addTodo' />
                        <StyledButton variant='add' onClick={addTask}>Add</StyledButton>
                    </CreateTaskWrapper>
                    <StyledList>
                        <TodoList/>
                    </StyledList>
                </TodoListWrapper>
            </AppWrapper>
        </ThemeProvider>
    );
};

export { App };