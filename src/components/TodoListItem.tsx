import { StyledButton, StyledCheckbox, StyledTodoTitle, TodoListItemWrapper } from '../style/style';
import { ITodo } from '../types/types';
import { useAppDispatch } from '../hook';
import { deleteTodo, toggleTodo } from '../store/todoSlice';

const TodoListItem: React.FC<ITodo> = ({id, title, completed}) => {
    const dispatch = useAppDispatch();
    return (
        <TodoListItemWrapper>
                <StyledCheckbox checked={completed}>
                    <span className='fake'></span>
                    <input checked={completed} onChange={() => dispatch(toggleTodo(id))} type='checkbox' />
                </StyledCheckbox>
                <StyledTodoTitle checked={completed}>{title}</StyledTodoTitle>
            <StyledButton variant='delete' onClick={() => dispatch(deleteTodo(id))}>Delete</StyledButton>
        </TodoListItemWrapper>
    );
};

export {TodoListItem};