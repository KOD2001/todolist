import { useAppSelector } from '../hook';
import { TodoListItem } from './TodoListItem';


const TodoList: React.FC = () => {
    const items = useAppSelector(state => state.todos.list);
    return (
        <>
            {items.map(todo => {
                return <TodoListItem
                    key={todo.id}
                    {...todo}
                />
            })}
        </>
    );
};

export {TodoList};