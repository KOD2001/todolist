export interface ITodo {
    id: number;
    title: string;
    completed: boolean;
}

export interface ITheme {
    name: string,
    lightBg: string,
    darkBg: string,
    textColor: string,
    inputTextColor: string,
    buttonHover: string
}

export interface ITodoState {
    list: ITodo[],
    theme: ITheme
}