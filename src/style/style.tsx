import styled, { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle`
  @font-face {
    font-family: 'Segoe';
    src: url(/fonts/SegoeUI.ttf) format('ttf');
    font-weight: 400;
  }
  @font-face {
    font-family: 'Segoe';
    src: url(/fonts/SegoeUIBold.ttf) format('ttf');
    font-weight: 700;
  }
  * {
      font-family: 'Segoe', 'Roboto', sans-serif;
      margin: 0;
      padding 0;
      box-sizing: border-box;
  }
`

export const AppWrapper = styled.div`
    position: fixed;
    display: flex;
    justify-content: center;
    height: 100vh;
    width: 100%;
    background-color: ${props => props.theme.lightBg};
`
export const TodoListWrapper = styled.div`
    padding: 1rem;
    display: flex;
    align-items: center;
    flex-direction: column;
    background-color: ${props => props.theme.darkBg};
    height: 95vh;
    width: 60%;
    @media (max-width: 725px) {
      width: 88%;
    }
`
export const StyledList = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  overflow-y: auto;
  width: 650px;
  @media (max-width: 725px) {
    width: 350px;
  }

  &::-webkit-scrollbar {
    width: 5px;
  }
  &::-webkit-scrollbar-track {
    background-color: ${props => props.theme.lightBg};
  }
  &::-webkit-scrollbar-thumb {
    background-color: ${props => props.theme.textColor}; 
    transition: .2s;
    &:hover {
      background-color: rgb(47, 88, 99);
    }
}
`
export const TodoListItemWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  background-color: ${props => props.theme.lightBg};
  color: ${props => props.theme.textColor}; 
  width: 600px;
  @media (max-width: 725px) {
    width: 300px;
    font-size: 16px;
  }
  padding: 0.5rem;
  margin: 0.5rem 0;
  font-size: 20px;
  transition: border-color 0.3s ease-in-out, box-shadow 0.15s ease-in-out;
  &:hover {
    box-shadow: 0 0 0 0.2rem ${props => props.theme.textColor}; 
    cursor: pointer;
  }
`
export const StyledHeader = styled.h1`
    margin-top: 5vh;
    font-size: 46px;
    font-weight: 700;
    color: ${props => props.theme.textColor}; 
`
export const CreateTaskWrapper = styled.div`
    margin: 8vh 0 8vh 0;
`
export const StyledInput = styled.input`
    background-color: ${props => props.theme.lightBg};
    color: ${props => props.theme.inputTextColor};
    margin-right: 1rem;
    padding: 0.375rem 0.75rem;
    outline: none;
    border: none;
    font-size: 20px;
    transition: border-color 0.3s ease-in-out, box-shadow 0.15s ease-in-out;
    &:focus {
        box-shadow: 0 0 0 0.2rem ${props => props.theme.textColor}; 
    }
`
export const StyledButton = styled.button<{variant: string}>`
    font-size: 1rem;
    color: ${props => props.variant === 'delete' ? 'rgb(194, 19, 19)' : props.theme.textColor};
    padding: 0 16px;
    height: 5vh;
    background-color: ${props => props.theme.lightBg};
    outline: none;
    border: none;
    transition: background-color 0.2s ease-in-out, box-shadow 0.15s ease-in-out;
    &:hover {
        box-shadow: ${props => props.variant === 'delete' ? '0 0 0 0.2rem rgb(217, 43, 43)' : `0 0 0 0.2rem ${props.theme.textColor}`};
        background-color: ${props => props.variant === 'delete' ? 'rgb(247, 178, 178)' : props.theme.buttonHover};
        cursor: pointer;
    }
    &:focus {
        box-shadow: ${props => props.variant === 'delete' ? '0 0 0 0.2rem rgb(217, 43, 43)' : `0 0 0 0.2rem ${props.theme.textColor}`};
    }
`
export const StyledCheckbox = styled.label<{checked: boolean}>`
    display: inline-flex;
    align-items: center;
    & input {
      position: absolute;
      z-index: -1;
      opacity: 0;
    }
    & .fake {
      display: inline-block;
      width: 22px;
      height: 22px;
      position: relative;
      background-color: ${props => props.theme.darkBg};
      cursor: pointer;
      &::before {
        content: '';
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        display: block;
        width: 13px;
        height: 13px;
        background-color: ${props => props.theme.textColor};
        transition: .4s;
        opacity: ${props => props.checked ? '1' : '0'};
      }
    }
`
export const ThemeCheckbox = styled.label`
    position: absolute;
    display: inline-block;
    width: 100px;
    height: 34px;
    top: 2%;
    left: 82%;
    @media (max-width: 725px) {
      left: 37%;
    }
    & input {
      opacity: 0;
      width: 0;
      height: 0;
      &:checked + .slider {
        background-color: ${props => props.theme.textColor};
      }
      &:focuse + .slider {
        box-shadow: 0 0 1px ${props => props.theme.textColor};
      }
      &:checked + .slider::before {
        -webkit-transform: translateX(50%);
        -ms-transform: translateX(50%);
        transform: translateX(50%);
      }
    }
    & .slider {
      position: absolute;
      cursor: pointer;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      background-color: ${props => props.theme.textColor};
      -webkit-transition: .4s;
      transition: .4s;
      border-radius: 2%;
      &::before {
        position: absolute;
        content: '${props => props.theme.name}';
        color: ${props => props.theme.lightBg};
        height: 26px;
        width: 90px;
        left: 10%;
        bottom: 2%;
        -webkit-transition: .4s;
        transition: .4s;
      }
    }
`
export const StyledTodoTitle = styled.span<{checked: boolean}>`
    text-decoration-line: ${props => props.checked ? 'line-through' : 'none'};
    display: block;
    width: 450px;
    @media (max-width: 725px) {
      width: 170px;
    }
    overflow-wrap: break-word;
    word-wrap: break-word;
    white-space: normal;
`