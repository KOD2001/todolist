const darkTheme = {
    name: 'dark',
    lightBg: 'rgb(40,44,52)',
    darkBg: '#20232a',
    textColor: '#61dafb',
    inputTextColor: '#b9c4c7',
    buttonHover: 'rgb(47, 88, 99)'
}

const lightTheme = {
    name: 'light',
    lightBg: '#dce5e8',
    darkBg: 'rgb(188, 203, 224)',
    textColor: 'rgb(76, 101, 217)',
    inputTextColor: 'rgb(40,44,52)',
    buttonHover: 'rgb(163, 201, 255)'
}

export {darkTheme, lightTheme};